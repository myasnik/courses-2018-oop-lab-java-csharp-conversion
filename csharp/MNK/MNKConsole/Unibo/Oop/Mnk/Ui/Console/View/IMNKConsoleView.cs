using Unibo.Oop.Utils;

namespace Unibo.Oop.Mnk.Ui.Console.View
{
    public interface IMNKConsoleView
    {
        IMNKMatch match { get; set; }

        void RenderNextTurn(IMNKMatch model, int turn, Symbols player, IImmutableMatrix<Symbols> state);
        
        void RenderEnd(IMNKMatch model, int turn, Symbols? winner, IImmutableMatrix<Symbols> state);
    }
}