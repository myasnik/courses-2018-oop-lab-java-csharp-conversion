using System;
using System.IO;

namespace Unibo.Oop.Mnk.Ui.Console.Control
{
    internal class MNKConsolControlImpl : IMNKConsoleControl
    {
        public IMNKMatch Model { get; set; }

        public void Input()
        {
            try
            {
                if (Model != null)
                {
                    while (true)
                    {
                        string line = System.Console.ReadLine();
                        string[] coords = System.Text.RegularExpressions.Regex.Split(line, @"\s{2,}"); // split using one or more spaces as delimiters
                        
                        if (line.Equals("reset", StringComparison.OrdinalIgnoreCase))
                        {
                            Model.Reset();
                            break;
                        }
                        else if (line.Equals("exit"))
                        {
                            Environment.Exit(0);
                        }
                        else if (coords.Length == 2)
                        {
                            string fst = coords[0];
                            string snd = coords[1];
                            int i = fst[0] - 'a';

                            try
                            {
                                int j = Int32.Parse(snd) - 1;
                                MakeMove(i, j);
                                break;
                            }
                            catch (Exception e)
                            {
                                System.Console.WriteLine(e.Message);
                            }
                        }
                        else
                        {
                            System.Console.WriteLine("Wrong syntax!");
                        }
                    }
                }
            }
            catch (IOException e)
            {
                System.Console.WriteLine(e.StackTrace);
            }
        }

        public void MakeMove(int i, int j)
        {
            if (Model != null)
            {
                Model.Move(i, j);
            }
        }
    }
}