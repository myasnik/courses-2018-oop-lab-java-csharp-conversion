using System;

namespace Unibo.Oop.Mnk.Ui.Console.Control
{
    public static class MNKConsoleControl
    {
        public static IMNKConsoleControl Of(IMNKMatch match)
        {
            IMNKConsoleControl control = new MNKConsolControlImpl();
            control.Model = match;
            return control;
        }
        
        public static IMNKConsoleControl Of()
        {
            return new MNKConsolControlImpl();
        }
    }
}