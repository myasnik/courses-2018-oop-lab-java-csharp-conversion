using System;

namespace Unibo.Oop.Mnk.Ui.Console.View
{
    public static class MNKConsoleView
    {
        public static IMNKConsoleView Of(IMNKMatch match)
        {
            IMNKConsoleView view = new MNKConsoleConsoleViewImpl();
            view.match = match;
            return view;
        }
        
        public static IMNKConsoleView Of()
        {
            return new MNKConsoleConsoleViewImpl();
        }
    }
}