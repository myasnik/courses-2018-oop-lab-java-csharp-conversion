﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Unibo.Oop.Utils;

namespace Unibo.Oop.Mnk.Ui.Console.View
{
    class MNKConsoleConsoleViewImpl : IMNKConsoleView
    {
        public IMNKMatch match { get => match; set => match = value; }

        public void RenderEnd(IMNKMatch model, int turn, Symbols? winner, IImmutableMatrix<Symbols> state)
        {
            System.Console.WriteLine("GAME OVER!");
            if (winner.HasValue)
            {
                System.Console.WriteLine(winner.Value + " wins!\n");
            }
            else
            {
                System.Console.WriteLine();
            }
        }

        public void RenderNextTurn(IMNKMatch model, int turn, Symbols player, IImmutableMatrix<Symbols> state)
        {
            System.Console.Write($"MNK-Game {state.RowsCount}x{state.ColumnsCount}x{model.K} -- Turn {turn} -- Player: {player.GetName()}\n");

            System.Console.Write("\t\t  ");
            System.Console.WriteLine(
                    Enumerable.Range(1, state.ColumnsCount)
                            .Select(c => c.ToString())
                            .ToString()
            );

            for (int i = 0; i < state.RowsCount; i++)
            {
                System.Console.Write("\t\t");
                System.Console.Write((char)i);
                System.Console.Write(" ");
                System.Console.WriteLine(renderRow(state, i));
            }

            System.Console.Write($"\tWhere do you want to put a {player}? (pattern: <row> <column>)\n\t> ");

        }

        private String renderRow(IImmutableMatrix<Symbols> state, int i)
        {
            return state.GetRow(i).Select(c => c.ToString()).ToString();
        }
    }
}
