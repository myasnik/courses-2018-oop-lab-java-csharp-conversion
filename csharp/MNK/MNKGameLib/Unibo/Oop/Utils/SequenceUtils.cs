using System;
using System.Collections.Generic;
using System.Linq;

namespace Unibo.Oop.Utils
{
    public static class SequenceUtils
    {
        // TODO consider using the `yield return` feature
        public static IEnumerable<Tuple<int, int, TElem>> Subsequences<TElem>(this IEnumerable<TElem> sequence) 
            where TElem : struct // TODO notice me, ask for me
        {
            var i = 0;
            TElem? symbol = null;
            var index = -1;
            var length = 0;

            foreach (var e in sequence)
            {
                if (Object.Equals(e, symbol))
                {
                    length++;
                }
                else
                {
                    if (symbol != null)
                    {
                        yield return Tuple.Create(index, length, symbol.Value);
                    }
                    symbol = e;
                    index = i;
                    length = 1;
                }
                i++;
            }

            if (symbol != null)
            {
                yield return Tuple.Create(index, length, symbol.Value);
            }
        }
    }
}