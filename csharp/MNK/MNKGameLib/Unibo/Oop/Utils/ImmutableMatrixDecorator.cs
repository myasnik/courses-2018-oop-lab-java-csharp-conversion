using System;
using System.Collections;
using System.Collections.Generic;

namespace Unibo.Oop.Utils
{
    public class ImmutableMatrixDecorator<TElem> : IImmutableMatrix<TElem>
    {
        private readonly IBaseMatrix<TElem> decorated;

        public ImmutableMatrixDecorator(IBaseMatrix<TElem> decorated) 
        {
            this.decorated = decorated ?? throw new InvalidOperationException();
        }

        public TElem this[int i, int j] => this[i, j];

        public int ColumnsCount => decorated.ColumnsCount;

        public int RowsCount => decorated.RowsCount;

        public int Count => decorated.Count;

        public int CoordAntidiagonal(int i, int j)
        {
            return decorated.CoordAntidiagonal(i, j);
        }

        public int CoordColumn(int d, int a)
        {
            return decorated.CoordColumn(d, a);
        }

        public int CoordDiagonal(int i, int j)
        {
            return decorated.CoordDiagonal(i, j);
        }

        public int CoordRow(int d, int a)
        {
            return decorated.CoordRow(d, a);
        }

        public void ForEachIndexed(Action<int, int, TElem> consumer)
        {
            decorated.ForEachIndexed(consumer);
        }

        public IEnumerable<TElem> GetAntidiagonal(int a)
        {
            return decorated.GetAntidiagonal(a);
        }

        public IEnumerable<TElem> GetColumn(int j)
        {
            return decorated.GetColumn(j);
        }

        public IEnumerable<TElem> GetDiagonal(int d)
        {
            return decorated.GetDiagonal(d);
        }

        public TElem GetDiagonals(int d, int a)
        {
            return decorated.GetDiagonals(d, a);
        }

        public IEnumerator<TElem> GetEnumerator()
        {
            return GetEnumerator();
        }

        public IEnumerable<TElem> GetRow(int i)
        {
            return decorated.GetRow(i);
        }

        public TElem[] ToArray()
        {
            return decorated.ToArray();
        }

        public IList<TElem> ToList()
        {
            return decorated.ToList();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return decorated.GetEnumerator();
        }
    }
}