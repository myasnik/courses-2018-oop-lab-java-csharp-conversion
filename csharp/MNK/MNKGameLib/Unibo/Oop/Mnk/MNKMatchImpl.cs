using System;
using System.Collections.Generic;
using System.Linq;
using Unibo.Oop.Utils;

namespace Unibo.Oop.Mnk
{
    public class MNKMatchImpl : IMNKMatch
    {
        private readonly IMatrix<Symbols> grid;
        private int turn;
        private bool ended = false;
        private Symbols player;

        public MNKMatchImpl(int m, int n, int k)
            : this(m, n, k, SymbolsExtensions.PickRandomly())
        {
            
        }
        
        public MNKMatchImpl(int m, int n, int k, Symbols first) {
            if (k > Math.Min(m, n)) {
                throw new IndexOutOfRangeException("Required: k <= min(m, n)");
            }
            this.grid = Matrix.Of(m, n, Symbols.EMPTY);
            this.K = k;
            this.player = first;
            this.turn = 1;
        }

        public IImmutableMatrix<Symbols> Grid => ImmutableMatrix.Wrap(grid);

        public int K { get; }
        
        protected Symbols? CheckVictory(int i, int j, Symbols currentPlayer) {
            int d = grid.CoordDiagonal(i, j);
            int a = grid.CoordAntidiagonal(i, j);

            var res = new[] // TODO notice this syntax, ask about it!
                {
                    grid.GetRow(i),
                    grid.GetColumn(j),
                    grid.GetDiagonal(d),
                    grid.GetAntidiagonal(a)
                }.Select(this.KAligned)
                .Where(it => it != null)
                .Select(it => new Symbols?(it.Item3))
                .FirstOrDefault();

            return res;
        }
        
        protected Tuple<int, int, Symbols> KAligned(IEnumerable<Symbols> data)
        {
            //TODO
            List<Tuple<int, int, Symbols>> lst = SequenceUtils.Subsequences(data);
            return data
                .Where(t => t.)
                .Where(t => t. >= K)
                .findAny();
        }
        
        private void BeforeMoving(int turn, int i, int j, Symbols currentPlayer) {
            if (grid[i, j] != Symbols.EMPTY)
            {
                throw new InvalidOperationException($"Cell ({i}, {j}) is not empty!");
            }
        }
        
        private void AfterMoved(int turn, int i, int j, Symbols currentPlayer) {
            Nullable<Symbols> winner = CheckVictory(i, j, currentPlayer);
            if (winner.HasValue || turn >= grid.Count)
            {
                ended = true;
                OnMatchEnded(turn, currentPlayer, winner, i, j);
            }
            else
            {
                OnTurnEnded(turn, currentPlayer, i, j);
            }
        }

        private void OnMatchEnded(int currentTurn, Symbols currentPlayer, Symbols? winner, int i, int j)
        {
            MatchEnded?.Invoke(new MatchEventArgs(this, currentTurn, currentPlayer, winner, i, j));
        }
        
        private void OnTurnEnded(int currentTurn, Symbols currentPlayer, int i, int j)
        {
            TurnEnded.Invoke(new TurnEventArgs(this, currentTurn, currentPlayer, i, j));
            turn++;
            player = player.NextPlayer(); 
            TurnBeginning.Invoke(new TurnEventArgs(this, Turn, CurrentPlayer, i, j));
        }

        public void Move(int i, int j)
        {
            try
            {
                if (ended)
                {
                    throw new InvalidOperationException("Match is over!");
                }

                BeforeMoving(turn, i, j, player);
                grid[i, j] = player;
                AfterMoved(turn, i, j, player);
            }
            catch (Exception e)
            {
                ErrorOccurred?.Invoke(e);
            }
        }

        public void Reset()
        {
            grid.SetAll(Symbols.EMPTY);
            player = turn % 2 == 0 ? player.NextPlayer() : player;
            turn = 1;
            ended = false;
            ResetPerformed?.Invoke(this);
        }

        public int Turn => turn;

        public Symbols CurrentPlayer => player;
        
        public event Action<TurnEventArgs> TurnBeginning;

        public event Action<TurnEventArgs> TurnEnded;

        public event Action<MatchEventArgs> MatchEnded;

        public event Action<Exception> ErrorOccurred;

        public event Action<IMNKMatch> ResetPerformed;

    }
}