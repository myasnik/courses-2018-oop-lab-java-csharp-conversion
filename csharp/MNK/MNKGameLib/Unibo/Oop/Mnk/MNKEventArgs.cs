using System;
using Unibo.Oop.Utils;

namespace Unibo.Oop.Mnk
{
    public abstract class MNKEventArgs
    {
        private IMNKMatch source;
        private int turn;               //TODO?
        private Symbols player;
        private Tuple<int, int> move;

        public MNKEventArgs(IMNKMatch source, int turn, Symbols player, int i, int j)
        {
            this.source = source;
            this.turn = turn;
            this.player = player;                       //TODO?
            this.move = Tuple.Create(i, j);
        }

    }
}