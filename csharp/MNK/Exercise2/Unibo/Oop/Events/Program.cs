﻿using System.Collections.Generic;
using System;

namespace Unibo.Oop.Events
{
    public static class Program
    {
        private static readonly IList<string> emitted = new List<string>();

        public static void Main(string[] args) 
        {
            IEventEmitter<string> emitter = EventEmitter.Ordered<string>();

            Assert(emitted.Count == 0);

            emitter.Emit("attempt 1");

            Assert(emitted.Count == 0);

            emitter.EventSource.Bind(msg => emitted.Add(msg));

            emitter.Emit("attempt 2");

            Assert(emitted.Count == 1);
            Assert(emitted[0] == "attempt 2");

            emitter.EventSource.Bind(msg => emitted.Add(msg + "'"));

            emitter.Emit("attempt 3");

            Assert(emitted.Count == 3);
            Assert(emitted[0] == "attempt 2");
            Assert(emitted[1] == "attempt 3");
            Assert(emitted[2] == "attempt 3'");

            Console.WriteLine("OK");
            Console.ReadLine();
        }

        private static void Assert(bool expr, string msg = "Assertion failed") 
        {
            if (!expr) 
            {
                throw new Exception(msg);
            }
        }
    }
}
