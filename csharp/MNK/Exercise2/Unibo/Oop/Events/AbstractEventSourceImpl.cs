﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unibo.Oop.Events
{
    abstract class AbstractEventSourceImpl<TArg> : IEventSource<TArg>, IEventEmitter<TArg>
    {
        protected abstract ICollection<EventListener<TArg>> getEventListeners();
        public void Bind(EventListener<TArg> eventListener)
        {
            getEventListeners().Add(eventListener);
        }

        public void Unbind(EventListener<TArg> eventListener)
        {
            getEventListeners().Remove(eventListener);
        }

        public void UnbindAll()
        {
            getEventListeners().Clear();
        }

        public void Emit(TArg data)
        {
            foreach (EventListener<TArg> e in getEventListeners())
            {
                e.Invoke(data);
            }
        }

        public IEventSource<TArg> EventSource
        {
            get
            {
                return this;
            }
        }
    }
}
