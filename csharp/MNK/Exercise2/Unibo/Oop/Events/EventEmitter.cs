﻿using System;
namespace Unibo.Oop.Events
{
    public static class EventEmitter
    {
        public static IEventEmitter<TArg> Ordered<TArg>() 
        {
            return new OrderedEventSourceImpl<TArg>();
        }
    }
}
