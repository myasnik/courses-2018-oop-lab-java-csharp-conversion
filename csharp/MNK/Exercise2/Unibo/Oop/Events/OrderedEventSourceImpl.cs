﻿using System;
using System.Collections.Generic;

namespace Unibo.Oop.Events
{
    internal class OrderedEventSourceImpl<TArg> : AbstractEventSourceImpl<TArg>
    {
        private readonly IList<EventListener<TArg>> eventListeners = new List<EventListener<TArg>>();
        protected override ICollection<EventListener<TArg>> getEventListeners()
        {
            return eventListeners;
        }
    }
}
