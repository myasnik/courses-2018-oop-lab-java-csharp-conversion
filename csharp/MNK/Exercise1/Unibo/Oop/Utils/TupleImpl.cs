﻿using System;
using System.Linq;

namespace Unibo.Oop.Utils
{
    internal class TupleImpl : ITuple
    {
        private readonly object[] items;
        protected TupleImpl(object[] args)
        {
            items = args;
        }

        public object this[int i] => items[i];

        public int Length => items.Length;

        public object[] ToArray()
        {
            return items.ToArray();
        }

        override public string ToString()
        {
            string ret = "";
            bool first = true;
            foreach (object elem in items)
            {
                if (first)
                {
                    ret = ret + "(" + elem.ToString();
                    first = false;
                }
                else ret = ret + ", " + elem.ToString();
            }
            return ret + ")";
        }

        override public int GetHashCode()
        {
            int prime = 31;
            int result = 1;
            foreach (Object obj in  items)
            {
                result = prime * result + obj.GetHashCode();
            }
            return result;
        }

        private bool ArrayEq(object[] items, object[] items1)
        {
            if (items.Length != items1.Length) return false;
            for (int i = 0; i < items.Length; i++)
            {
                if (!items[i].Equals(items1[i])) return false;
            }
            return true;
        }

        override public bool Equals(Object obj)
        {
            return obj != null
                    && obj is TupleImpl
                    && ArrayEq(items, ((TupleImpl)obj).items);
        }
    }
}
